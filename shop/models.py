from django.db import models



class Article(models.Model):
    name = models.CharField(max_length=30)
    created_on = models.DateTimeField(auto_now_add=True)


class Product(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)
    image_url = models.CharField(max_length=900)
    price = models.FloatField()
    quantity = models.IntegerField()
    category = models.CharField(max_length=30)
    subcategory = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Cart(models.Model):
    user = models.CharField(max_length=30)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()

    def __str__(self):
        return self.user


class Order(models.Model):
    user = models.CharField(max_length=30)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user



class Contact(models.Model):
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    email = models.EmailField(max_length=254)
    message_subject = models.CharField(max_length=100, blank=True)
    message_body = models.TextField(max_length=3000, blank=True)

