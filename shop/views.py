from django.core.mail import send_mail
from django.shortcuts import render
from shop.models import Product, Contact

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views import generic as generic_views

from shop.forms import ContactForm
from .models import Product  # Import your Product model
class IndexView(generic_views.TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['products'] = Product.objects.all()
        return context
@login_required  # Requires user to be logged in to access this view
def wishlist_view(request):
    # Your view logic for the wishlist page goes here
    return render(request, 'wishlist.html')  # Replace 'wishlist.html' with your actual template

@login_required  # Requires user to be logged in to access this view
def cart_view(request):
    # Your view logic for the cart page goes here
    return render(request, 'cart.html')  # Replace 'cart.html' with your actual template

def all_products(request):
    products = Product.objects.all()
    return render(request, 'all_products.html', {'products': products})

def blog(request):

    return render(request, 'blog.html')
def about(request):

    return render(request, 'about.html')

def contact_page(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            # Create a new Contact instance and populate it with the form data
            contact = Contact(
                first_name=form.cleaned_data['first_name'],
                last_name=form.cleaned_data['last_name'],
                email=form.cleaned_data['email'],
                message_subject=form.cleaned_data['message_subject'],
                message_body=form.cleaned_data['message_body']
            )

            # Save the contact to the database
            contact.save()

            # Section for E-mail sending:
            subject = 'New Contact Submission from ' + form.cleaned_data['first_name']
            # message = 'You have received a new contact form submission from ' + form.cleaned_data['first_name'] + ' ' + \
            #           form.cleaned_data['last_name'] + '. Email: ' + form.cleaned_data[
            #               'email'] + '. Message Subject: ' + \
            #           form.cleaned_data['message_subject'] + '. Message Body: ' + form.cleaned_data['message_body']
            # send_mail(subject, message, 'iv.svetlin@outlook.com', ['isvetllin@gmail.com'])
            # End of E-mail sending section.

            # Redirect or show a success message
            return render(request, 'contact_success.html')
    else:
        form = ContactForm()

    return render(request, 'contact_page.html', {'form': form})