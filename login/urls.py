from django.urls import path
from login import views
from django.views import generic as generic_views



urlpatterns = [
    path('logout/', views.logout_view, name='logout'),
    path('login/', views.login_view, name='login'),
    path('home/', views.home, name='home'),    # Matches /home/ URL
]
