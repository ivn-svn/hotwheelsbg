from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django import forms
import random
from django.contrib.auth.models import User



class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


def login_view(request):
    if request.method == 'POST':
        # Assuming you have a custom LoginForm defined in forms.py
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            # Authenticate the user
            user = authenticate(username=username, password=password)

            if user is not None:
                # Log the user in if authentication is successful
                login(request, user)
                return render(request, 'home.html')  # Redirect to home page after successful login
            else:
                # Authentication failed, handle the error accordingly
                error_message = "Invalid username or password."
        else:
            # Form data is invalid, handle the error accordingly
            error_message = "Invalid form data. Please check the input fields."
    else:
        # If it's not a POST request, display an empty login form
        form = LoginForm()
        error_message = None

    return render(request, './login.html', {'form': form, 'error_message': error_message})


def logout_view(request):
    # Your logout view logic here
    return render(request, './logout.html')

def home(request):
    # Your logout view logic here
    return render(request, './home.html')
def index(request):
    suffix = random.randint(1, 1000000)
    User.objects.create(
        username=f'admin_{suffix}',
        password='*31321sdJ',
    )
    # Your logout view logic here
    return render(request, './index.html')