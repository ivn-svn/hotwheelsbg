from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView
from blog.models import Article

class ArticleListView(ListView):
    model = Article
    template_name = 'article_list.html'  # Create this template
    context_object_name = 'articles'
    ordering = ['-publication_date']
