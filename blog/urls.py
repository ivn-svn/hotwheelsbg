from django.urls import path
from blog.views import ArticleListView

urlpatterns = [
    path('article_list/', ArticleListView.as_view(), name='article_list'),
]
