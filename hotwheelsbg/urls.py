from django.contrib import admin

from django.urls import path, include
from login import views
from django.views import generic as generic_views
import shop.views as shop_views

from blog.views import ArticleListView
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', shop_views.all_products, name='all_products'),      # Matches the root URL
    path('blog/', shop_views.blog, name='blog'),
    path('about/', shop_views.about, name='about'),
    path('contact_page/', shop_views.contact_page, name='contact_page'),
    path('admin/', admin.site.urls),
    path('blog/', include('blog.urls')),  # Include your app's URL patterns here
    path('article_list/', ArticleListView.as_view(), name='article_list'),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)